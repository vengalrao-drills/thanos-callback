// Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.

const fs = require("fs");
const path = require("path");

const allListsOfId = (boardId, callback) => {
  const filePath = path.join(__dirname, "data", "lists_1.json");
  fs.readFile(filePath, "utf-8", (err, data) => { // reading file
    if (err) {
      callback(err);
    } else {
      try {
        let listData = JSON.parse(data);   //converting a JSON-formatted string into a JavaScript object.
        listData = Object.entries(listData);
        // console.log(listData)
        let findAns = listData.reduce((accumulator, current) => {
          if (current[0] == boardId) { //matching the id's
            accumulator = JSON.stringify(current[1]);
          }
          return accumulator;
        }, {});
        // console.log(findAns);
        callback(null, findAns);
      } catch (err) {
        callback(err);
      }
    }
  });
};
module.exports = allListsOfId;

// allListsOfId("mcu453ed", (err, data) => {
//   if (err) {
//     console.log(err);
//   } else {
//     console.log(data);
//   }
// });
