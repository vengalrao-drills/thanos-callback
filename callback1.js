// Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.

const fs = require("fs");
const path = require("path");

const getBoardsInformation = (boardId, callback) => {
  const filepath = path.join(__dirname, "data", "boards_1.json");
  // console.log(filepath)
  fs.readFile(filepath, "utf-8", (err, data) => {
    if (err) {
      callback(err);
    } else {
      try {
        //recieved data is in the form of string. so using JSON.parse(data)
        const boardsData = JSON.parse(data);
        // console.log(typeof data)

        // using reduce function. to find out the board id.
        let id = boardsData.reduce((accumulator, current) => {
          if (current.id == boardId) { // if borad id == current id
            accumulator = current; // then assign it to the accumulator
          }
          return accumulator;
        }, {});
        id = JSON.stringify(id); // returning the string. not come  output as[object] - so stringify

        callback(null, `${id}`);
      } catch (err) {
        callback(err);
      }
    }
  });
};

module.exports = getBoardsInformation;
