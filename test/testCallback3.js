// Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.

const allCards = require("../callback3.js");
allCards("qwsa221", (err, data) => {
  if (err) {
    console.log(err);
  } else {
    console.log("testId :- qwsa221\n cards", data);
  }
});
