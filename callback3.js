// Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.

const fs = require("fs");
const path = require("path");

const allCards = (listID, callback) => {
  const filePath = path.join(__dirname, "data", "cards_1.json");
  fs.readFile(filePath, "utf-8", (err, data) => {
    if (err) {
      callback(null);
    } else {
      try { // try -if any error
        let cardsData = JSON.parse(data);
        // console.log(typeof cardsData)

        cardsData = Object.entries(cardsData); // cards data is in object . so converting to array.
        // console.log(cardsData)

        let finalAns = cardsData.reduce((accumulator, current) => {
          if (current[0] == listID) { // ,matching the ids and assigning it to the accumulator
            accumulator = current[1]; 
          }
          return accumulator;
        }, []);
        callback(null,finalAns); //callback
      } catch (err) {
        callback(`code is having error -, ${err}`);
      }
    }
  });
};
 
 
module.exports = allCards;
