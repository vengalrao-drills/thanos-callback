/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

const fs = require("fs");
const path = require("path");

const previousFunction = (word, cardWordz, callback) => {
  let boardsPath = path.join(__dirname, "data", "boards_1.json");
  let finalAns = [];

  //     Get information from the Thanos boards
  fs.readFile(boardsPath, "utf-8", (err, data) => {
    if (err) {
      callback(err);
    } else {
      try {
        let boardsData = JSON.parse(data);
        let infoBoard = boardsData.reduce((accumulator, current) => {
          // console.log(current.name)
          if (current.name == word) {
            accumulator = current;
          }
          return accumulator;
        }, {});
        let value = { ...infoBoard };
        // finalAns.push(infoBoard)
        finalAns.push(JSON.stringify(value));

        // Get all the lists for the Thanos board
        let heroId = infoBoard.id; // getting the id from the boards_1.json.

        let listsPath = path.join(__dirname, "data", "lists_1.json");
        fs.readFile(listsPath, "utf-8", (err, data) => {
          if (err) {
            callback(err);
          } else {
            try {
              // console.log(data)
              let listsData = JSON.parse(data);
              listsData = Object.entries(listsData);
              // console.log(listsData)
              let infoLists = listsData.reduce((accumulator, current) => {
                if (current[0] == heroId) {
                  accumulator = current[1];
                }
                return accumulator;
              }, {});

              let value1 = [...infoLists];

              finalAns.push(JSON.stringify(value1));
              // console.log(infoLists)

              let cardsPath = path.join(__dirname, "data", "cards_1.json");
              let herosArray = infoLists;
              //   console.log(herosArray)
              //   console.log( herosArray)

              // Get all cards for the Mind and Space lists simultaneously
              fs.readFile(cardsPath, "utf-8", (err, data) => {
                if (err) {
                  callback(err);
                } else {
                  try {
                    cardWordz = cardWordz.split("and");
                    // console.log(cardWordz)

                    let cardIds = cardWordz.reduce((accumulator, element) => {
                      let cardId = herosArray.reduce((accumulator, current) => {
                        let word1 = current.name.trim().toLowerCase();
                        let word2 = element.trim().toLowerCase();
                        if (word1 == word2) {
                          accumulator = current.id;
                        }
                        return accumulator;
                      }, []);
                      accumulator.push(cardId);
                      return accumulator;
                    }, []);
                    // console.log(cardIds)

                    let cardsData = JSON.parse(data);
                    cardsData = Object.entries(cardsData);
                    // console.log(cardsData)

                    let lastData = cardIds.reduce(
                      (Cardsaccumulator, CardsCurrent) => {
                        let cardsAns = cardsData.reduce(
                          (accumulator, current) => {
                            let cardId = CardsCurrent;
                            if (current[0] == cardId) {
                              accumulator = current[1];
                            }
                            return accumulator;
                          },
                          []
                        );
                        Cardsaccumulator[CardsCurrent] = cardsAns;
                        return Cardsaccumulator;
                      },
                      {}
                    );
                    // console.log(cardsAns)
                    //
                    let value = lastData;
                    finalAns.push(JSON.stringify(value));
                    // console.log(cardsAns)
                    callback(null, finalAns);
                  } catch (err) {
                    callback(err);
                  }
                }
              });
            } catch (error) {
              callback(error);
            }
          }
        });
      } catch (err) {
        callback(err);
      }
    }
  });
};

module.exports = previousFunction;

// previousFunction("Thanos",'Mind and Space' ,(err, data) => {
//   if (err) {
//     console.log(err);
//   } else {
//     console.log(data);
//   }
// });
